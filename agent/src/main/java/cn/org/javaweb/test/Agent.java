package cn.org.javaweb.test;
/*
 * Copyright sky 2018-11-20 Email:sky@03sec.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.lang.instrument.Instrumentation;

/**
 * @author sky
 */
public class Agent {

	/**
	 * 编写 premain 函数
	 * Inst 是一个 java.lang.instrument.Instrumentation 的实例，由 JVM 自动传入。
	 * java.lang.instrument.Instrumentation 是 instrument 包中定义的一个接口，
	 * 也是这个包的核心部分，集中了其中几乎所有的功能方法，例如类定义的转换和操作等等。
	 *
	 * @param agentOps
	 * @param inst
	 */
	public static void premain(String agentOps, Instrumentation inst) {
		System.out.println("=======this is agent premain function=======");
		inst.addTransformer(new TestTransformer());
	}

}