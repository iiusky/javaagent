/*
 * Copyright yz 2016-01-14  Email:admin@javaweb.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.org.javaweb.test;

import java.io.*;

public class IOUtils extends org.apache.commons.io.IOUtils {

	/**
	 * InputStream 转字符串
	 *
	 * @param in
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	public static String inputStreamToString(InputStream in, String encoding) throws IOException {
		return new String(inputStreamToByteArray(in), encoding);
	}

	/**
	 * InputStream 转字符串
	 *
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static String inputStreamToString(InputStream in) throws IOException {
		return inputStreamToString(in, "UTF-8");
	}

	/**
	 * 输入流转字节流
	 *
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static byte[] inputStreamToByteArray(InputStream in) throws IOException {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			int                   a   = 0;
			byte[]                b   = new byte[1024];

			while ((a = in.read(b)) != -1) {
				out.write(b, 0, a);
			}

			return out.toByteArray();
		} catch (IOException e) {
			throw e;
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	/**
	 * 读取文件到字符串
	 *
	 * @param path     文件路径
	 * @param encoding 编码
	 * @return
	 * @throws IOException
	 */
	public static String readFileToString(String path, String encoding) throws IOException {
		return inputStreamToString(new FileInputStream(path));
	}

	public static String readFileToString(String path) throws IOException {
		return inputStreamToString(new FileInputStream(path));
	}

	/**
	 * Copies bytes from an {@link InputStream} <code>source</code> to a file
	 * <code>destination</code>. The directories up to <code>destination</code>
	 * will be created if they don't already exist. <code>destination</code>
	 * will be overwritten if it already exists.
	 *
	 * @param source      the <code>InputStream</code> to copy bytes from, must not
	 *                    be <code>null</code>
	 * @param destination the non-directory <code>File</code> to write bytes to
	 *                    (possibly overwriting), must not be <code>null</code>
	 * @throws IOException if <code>destination</code> is a directory
	 * @throws IOException if <code>destination</code> cannot be written
	 * @throws IOException if <code>destination</code> needs creating but can't
	 *                     be
	 * @throws IOException if an IO error occurs during copying
	 * @since 2.0
	 */
	public static void copyInputStreamToFile(InputStream source, File destination) throws IOException {
		FileOutputStream output = null;
		try {
			output = new FileOutputStream(destination);
			copy(source, output);
			output.close();
		} catch (IOException e) {
			throw e;
		} finally {
			IOUtils.closeQuietly(source);
			IOUtils.closeQuietly(output);
		}
	}

	/**
	 * 流关闭、Closeable 对象关闭
	 *
	 * @param closeable
	 */
	public static void closeQuietly(Closeable closeable) {
		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
