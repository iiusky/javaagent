/*
 * Copyright sky 2018-11-29 Email:sky@03sec.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.org.javaweb.test;


import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * @author sky
 */
public class TestClassAdapter extends ClassVisitor implements Opcodes {


	public TestClassAdapter(ClassVisitor classVisitor) {
		super(Opcodes.ASM5, classVisitor);
	}

	/**
	 * @param access
	 * @param methodName  方法名
	 * @param argTypeDesc 方法描述符
	 * @param signature
	 * @param exceptions
	 * @return
	 */
	@Override
	public MethodVisitor visitMethod(int access, String methodName, String argTypeDesc,
	                                 String signature, String[] exceptions) {


		MethodVisitor mv = super.visitMethod(access, methodName, argTypeDesc, signature, exceptions);
		//判断方法名，为了保证唯一性，真正使用时还需要判断方法描述符是否一致
		if ("main".equals(methodName)) {
			System.out.println(methodName + "方法的描述符是：" + argTypeDesc);
//			System.out.println("methodName is :" + methodName);
//
			return new MethodVisitor(api, mv) {

				@Override
				public void visitCode() {


					mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
					mv.visitIntInsn(BIPUSH, 20);
					mv.visitMethodInsn(INVOKESTATIC, "cn/org/javaweb/test/TestInsert", "hello", "(I)I", false);
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(I)V", false);

					mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
					mv.visitLdcInsn("\u6c6a\u6c6a\u6c6a");
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);


					super.visitCode();
				}

			};
		}
//		try {
//			FileWriter writer = new FileWriter("/Users/sky/tmp/class/test.log", true);
//			writer.write("调用方法：" + methodName + "\r\n");
//			writer.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		return mv;

	}
}
