/*
 * Copyright sky 2018-11-20 Email:sky@03sec.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.org.javaweb.test;


import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;


/**
 * @author sky
 */
public class TestTransformer implements ClassFileTransformer {


	/**
	 * 注册一个自定义的transform
	 *
	 * @param loader              class loader
	 * @param className           类名
	 * @param classBeingRedefined
	 * @param protectionDomain
	 * @param classfileBuffer
	 * @return
	 * @throws IllegalClassFormatException
	 */
	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
	                        ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {

		// asm中的包名格式为 cn/org/javaweb 需要将 / 转为 .
		className = className.replace("/", ".");
//		System.out.println("当前加载的类为：" + className);

		//如果类名中存在javaweb 则使用ClassReader读取字节码，然后在创建一个ClassWriter用于拼接字节码，
		// 之后在进入我们自定义的ClassVisitor

		if (className.contains("javaweb")) {

			ClassReader  classReader  = new ClassReader(classfileBuffer);
			ClassWriter  classWriter  = new ClassWriter(classReader, ClassWriter.COMPUTE_MAXS);
			ClassVisitor classVisitor = new TestClassAdapter(classWriter);

			classReader.accept(classVisitor, ClassReader.EXPAND_FRAMES);

			classfileBuffer = classWriter.toByteArray();
			String file = "/Volumes/Data/code/work/study/javaagent/agent/src/main/java/cn/org/javaweb/test/class/" + className + ".class";

			try {
				IOUtils.copy(new ByteArrayInputStream(classfileBuffer), new FileOutputStream(file));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return classfileBuffer;
	}
}